---
title: "[Netlify] #1 Simple Application"
published: 2024-04-14
description: 'Deploy an application on netlify'
image: '../../img/netlify-icon.svg'
tags: [Application, Website, Frontend, StartUp]
category: 'Platform'
draft: false 
---

# How to deploy?
1. Add new project link with gitlab project.
2. Setting configs
    1. Site configuration >> Build & deploy >> Build Settings
    2. Build command: pnpm run build (build command)
    3. Publish directory: dist (dict has index.html)


