---
title: '[Angular] #1 Starting Angular'
published: 2024-04-18
description: 'Angular Start'
image: '../../img/icons8-angular.svg'
tags: [Website, Angular, Frontend]
category: Development
draft: false 
---
cd ~/.ssh/
touch config
open config
# Install Angular CLI
```
sudo npm inatall -g @angular/cli@lastest
```

# How to start project
1. Create project
   ```
   ng new first-app
   ```
2. Activate project
    ```
    cd -first-app
    ng serve
    ```
3. See
   ```
   http://localhost:4200
   ```

# Angular setting

## Initial location
1. Open `angular.json`
2. See first startup files about index and browser
    ```
    "index": "src/index.html",
    "browser": "src/main.ts",
    ```
## Module location
- See `src/main.ts` can know modules are in `src/app/`

## Component location
- See `src/app/app.component.ts` can know  initial components

# Modify First Page
1. Open `src/app/app.component.html`
2. Delete all thing!
3. Add html
   ```html
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Start Angular</title>
        <link rel="stylesheet" href="app.component.css">
    </head>
    <body>
        Hi there
    </body>
    </html>
   ```