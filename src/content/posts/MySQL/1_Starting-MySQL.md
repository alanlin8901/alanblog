---
title: "[MySQL] #1 Starting MySQL"
published: 2024-04-16
description: 'Run MySQL at macos'
image: '../../img/mysql-icon.svg'
tags: [MySQL, StartUp]
category: 'Database'
draft: false 
---

`ENVIRONMENT: zshrc, macos, 32G RAM`

# Install MySQL Community Downloads
- install from official website


# Homebrew 
- Install homebrew `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
- add path `export HOMEBREW_NO_AUTO_UPDATE=true` to `~/.zshrc`

# Install MySQL (Fail 😢)
1. `brew install mysql mycli`
    - check service `brew services list`
2. `mysqld --initialize --explicit_defaults_for_timestamp`
3. Start service `brew services start mysql`
4. Login MySQL `mycli -u root -h localhost`
5. Stop service `brew services stop mysql`

# Uninstall MySQL

```
brew remove mysql
brew cleanup
launchctl unload -w ~/Library/LaunchAgents/homebrew.mxcl.mysql.plist
rm ~/Library/LaunchAgents/homebrew.mxcl.mysql.plist
sudo rm -rf /usr/local/var/mysql
```