---
title: "[SpringBoot] #2 Adding MySQL"
published: 2024-04-16
description: 'Use MySQL in spring boot project'
image: '../../img/icons8-spring-boot.svg'
tags: [Website, SpringBoot, Backend]
category: Development
draft: false 
---

`ENVIRONMENT: zshrc, macos, 32G RAM`

# Initial
- Install MySQL to OS

# Step
1. Add setting application.properties
   ```js
      spring.jpa.hibernate.ddl-auto=update
      spring.datasource.url=jdbc:mysql://${MYSQL_HOST:localhost}:3306/db_example
      spring.datasource.username=springuser
      spring.datasource.password=ThePassword
      spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
   ```

2. Add dependency to pom.xml
   ```js
      <dependency>
         <groupId>mysql</groupId>
         <artifactId>mysql-connector-java</artifactId>
         <version>8.0.33</version>
      </dependency>
   ```