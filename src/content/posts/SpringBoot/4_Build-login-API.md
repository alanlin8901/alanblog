---
title: "[SpringBoot] #4 Building Login API"
published: 2024-04-26
description: 'Build login API'
image: '../../img/icons8-spring-boot.svg'
tags: [Website, SpringBoot, Backend]
category: Development
draft: false 
---

# Steps
1. Create `AccessingDataMysqlApplication.java` 
    ```java
    package com.example.accessingdatamysql;
    @SpringBootApplication
    public class AccessingDataMysqlApplication {
        public static void main(String[] args) {
            SpringApplication.run(AccessingDataMysqlApplication.class, args);
        }
    }
    ```
2. Modify `MainController.java` about API linker
    ```java
    package com.example.accessingdatamysql;

    @Controller
    @RequestMapping(path="/demo")
    @CrossOrigin(origins = "*")
    public class MainController {
        @Autowired
        private UserRepository userRepository;
        private static final Logger logger = LoggerFactory.getLogger(MainController.class);

        @PostMapping(path="/login")
        public @ResponseBody String login(@RequestBody User loginRequest) {
            String email = loginRequest.getEmail();
            String password = loginRequest.getPassword();
            User user = userRepository.findByEmail(email);
            if (user != null && user.getPassword().equals(password)) {
                logger.info("API '/login' Success");
                return "Login successful!";
            } else {
                logger.info("API '/login' Fail");
                return "Login failed. Invalid email or password.";
            }
        }
    }
    ```
3. Create `UserRepository.java`
    ```java
    package com.example.accessingdatamysql;
    public interface UserRepository extends CrudRepository<User, Integer> {
        User findByEmail(String email);
    }
    ```
4. Create `User.java` for user interface
    ```java
    package com.example.accessingdatamysql;
    @Entity
    public class User {
        @Id
        @GeneratedValue(strategy=GenerationType.AUTO)
        private String email;
        private String password;
        public String getEmail() {
            return email;
        }
        public void setEmail(String email) {
            this.email = email;
        }
        public String getPassword() {
            return password;
        }
        public void setPassword(String password) {
            this.password = password;
        }
    }
    ```

## Then API can create in the backend and we can post the login API!