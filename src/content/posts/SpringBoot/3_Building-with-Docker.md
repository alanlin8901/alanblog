---
title: "[SpringBoot] #3 Building with docker"
published: 2024-04-16
description: 'Use docker in spring boot project'
image: '../../img/icons8-spring-boot.svg'
tags: [Website, SpringBoot, Backend]
category: Development
draft: false 
---

`ENVIRONMENT: zshrc, macos, 32G RAM`

# Compose
1. Create a docker file named docker-compose.yml
```bash
services:
  db:
    image: mysql
    restart: always
    ports:
      - "3306:3306"
    expose:
      - "3306"
    environment:
      - MYSQL_USER=springuser
      - MYSQL_PASSWORD=ThePassword
      - MYSQL_DATABASE=db_example
      - MYSQL_ROOT_PASSWORD=root
    volumes:
      - "./conf.d:/etc/mysql/conf.d:ro"
```
2. Add dependency
```js
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-docker-compose</artifactId>
</dependency>
```

3. Set up application.properties
```js
spring.jpa.generate-ddl=true
```

# Build dockerfile
First need to package the project, can use `mvn clean package`

```bash
FROM openjdk:21-oracle
COPY ["./target/accessing-data-mysql-complete-0.0.1-SNAPSHOT.jar", "/app/"]
COPY ["./env-config/prod", "/app/config/"]
EXPOSE 3306
WORKDIR /app
CMD ["java", "-Dspring.config.location=config/", "-jar", "backend-app.jar", "--spring.config.name=application"]
```

# Check

- application.properties of production 
```js
spring.jpa.hibernate.ddl-auto=update
spring.datasource.url=jdbc:mysql://db:3306/db_example
spring.datasource.username=springuser
spring.datasource.password=ThePassword
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.jpa.generate-ddl=true
logging.file.path=./log
#spring.jpa.show-sql: true
```
- port
  - docker-compose ports need compare to Dockerfile EXPOSE