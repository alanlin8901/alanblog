---
title: "[SpringBoot] #1 Starting SpringBoot"
published: 2024-04-11
description: A SpringBoot development first start.
image: '../../img/icons8-spring-boot.svg'
tags: [Website, SpringBoot, StartUp, Backend]
category: Development
draft: false
---
`ENVIRONMENT: zshrc, macos, 32G RAM`

# Prepare
## Install SDK
  - Add `curl -s "https://get.sdkman.io" | bash`
   
## Install JAVA
  - `sdk install java 21.0.2-open`
  - add `export "JAVA_HOME=\$(/usr/libexec/java_home)"` to `~/.zshrc`
  
## ** Install MAVEN
  - `sdk install maven`

# Create Spring Boot Project
1. Setup and create sprint boot project from [spring initializr](https://start.spring.io) 
     1. Java 21 >> 3.2.4 >> Jar
     2. Dependencies - Spring Web
2. Add hello world at `src/main/java/com/example/demo/DemoApplication.java`
```java
package com.example.demo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DemoApplication {
    public static void main(String[] args) {
      SpringApplication.run(DemoApplication.class, args);
    }
    @GetMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
      return String.format("Hello %s!", name);
    }
}
```

3. Execute
   - `./gradlew bootRun`
   - or `mvn spring-boot:run`
  
# RUN
- `http://localhost:8080/hello`

You can see the your first application with web in the localhost 😆
